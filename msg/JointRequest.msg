# This is a message that holds data to describe a request to a set of joints. 
# 
# Each joint is uniquely identified by its name.
# The header specifies the time at which te request is sent. All joint requests in one
# message are assumed to have the same time stamp.
#
# All arrays in this message should have the same size.This is the only way to uniquely
# associate the joint name with the correct setpoint and request type.

# Request type definitions.
int8 TYPE_DISABLED = -1
int8 TYPE_STOP     = 0
int8 TYPE_POSITION = 1  # rad or m
int8 TYPE_VELOCITY = 2  # rad/s or m/s
int8 TYPE_TORQUE   = 3  # Nm or N
int8 TYPE_VOLTAGE  = 4  # V

std_msgs/Header header
string requester        # Name of the requester.
uint32 priority         # Priority of the request. Higher values are prioritized. 

string[] name       # Joint names.
int8[] type         # Request types (values defined above).
float64[] setpoint  # Request setpoint.